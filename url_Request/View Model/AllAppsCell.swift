//
//  AllAppsCell.swift
//  url_Request
//
//  Created by Zaryab on 5/17/21.
//  Copyright © 2021 M Zaryab. All rights reserved.
//

import UIKit

class AllAppsCell: UITableViewCell {
    //MARK:-IBOutets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var appId: UILabel!
    @IBOutlet weak var appTimeStamp: UILabel!
    @IBOutlet weak var platform: UILabel!
    @IBOutlet weak var bundleId: UILabel!
    
    
    
    var allAppsViewModel : AllAppsViewModel!{
        didSet{
            
            appName.text = allAppsViewModel.appName
            appId.text = allAppsViewModel.appId
            bundleId.text = allAppsViewModel.appBundleId
            
            let platform = allAppsViewModel.platform
                
            switch platform?.count {
                case 1:
                    let platformFormat = "\(platform![0])"
                    self.platform.text = platformFormat
                case 2:
                    let platformFormat = "\(platform![0]),\(platform![1])"
                    self.platform.text = platformFormat
                case 3:
                    let platformFormat = "\(platform![0]),\(platform![1]),\(platform![2])"
                    self.platform.text = platformFormat
                default:
                    let platformFormat = "Nil"
                    self.platform.text = platformFormat
                }
            
            let appTimeStamp = allAppsViewModel.timestamp!

                let myDate = NSDate(timeIntervalSince1970: TimeInterval(appTimeStamp))
    //            cell.appTimeStamp.text = (myDate as NSNumber).stringValue
                let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yy"
                let formatteddate = formatter.string(from: myDate as Date)
                self.appTimeStamp.text = formatteddate
            
    }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.addShadow()
    }
    
}

