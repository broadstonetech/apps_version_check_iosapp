//
//  GetDetailCell.swift
//  url_Request
//
//  Created by Zaryab on 5/17/21.
//  Copyright © 2021 M Zaryab. All rights reserved.
//

import UIKit

class GetDetailCell: UITableViewCell {
    //MARK:- IBOutlet
    @IBOutlet weak var platform: UILabel!
    @IBOutlet weak var environment: UILabel!
    @IBOutlet weak var buildNo: UILabel!
    @IBOutlet weak var versionNumber: UILabel!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    var detailViewModel: DetailViewModel! {
        
        didSet{
            platform.text = detailViewModel.platform
            environment.text = detailViewModel.environment
            buildNo.text = detailViewModel.buildNo
            versionNumber.text = detailViewModel.versionNumber
            if (detailViewModel.comments.count == 1){
            comments.text = detailViewModel.comments[0]
            }
            else if (detailViewModel.comments.count == 2){
                comments.text = "\(detailViewModel.comments[0])\n \(detailViewModel.comments[1])"
            }else if (detailViewModel.comments.count == 3){
                comments.text = "\(detailViewModel.comments[0])\n\(detailViewModel.comments[1])\n\(detailViewModel.comments[2])"
            }
            
            
            else {comments.text = ""}
            
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.addShadow()
    }
    //MARK:- Methods
    
}
