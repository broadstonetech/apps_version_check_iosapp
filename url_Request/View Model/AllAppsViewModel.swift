//
//  AllAppsViewModel.swift
//  url_Request
//
//  Created by SaifUllah Butt on 28/06/2021.
//  Copyright © 2021 M Zaryab. All rights reserved.
//

import Foundation

class AllAppsViewModel {
    
    let appBundleId : String!
    let appId : String!
    let appName : String!
    let platform : [String]!
    let timestamp : Int!

    init(allApps: AllAppsData ) {
        self.appBundleId = allApps.appBundleId
        self.appId = allApps.appId
        self.appName = allApps.appName
        self.platform = allApps.platform
        self.timestamp = allApps.timestamp
    }
    
}
