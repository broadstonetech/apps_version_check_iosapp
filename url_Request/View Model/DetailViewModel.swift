//
//  DetailViewModel.swift
//  url_Request
//
//  Created by SaifUllah Butt on 27/06/2021.
//  Copyright © 2021 M Zaryab. All rights reserved.
//

import Foundation

class DetailViewModel{
    
    let platform: String
    let environment: String
    let buildNo: String
    let versionNumber: String
    let comments: [String]

   
    init(detail: DetailAppData) {
        self.platform = detail.platform
        self.environment = detail.environment
        self.buildNo = detail.buildNo
        self.versionNumber = detail.versionNo
        self.comments = detail.comments
        
        
    
    
    }
    
}
