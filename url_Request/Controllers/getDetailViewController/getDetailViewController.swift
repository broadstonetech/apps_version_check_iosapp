//
//  getDetailViewController.swift
//  url_Request
//
//  Created by Zaryab on 5/17/21.
//  Copyright © 2021 M Zaryab. All rights reserved.
//
// swiftlint:disable all
import UIKit

class getDetailViewController: UIViewController { // swiftlint:disable:this type_name
    // MARK:- IBoutlet
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: GetDetailCell.className, bundle: nil), forCellReuseIdentifier: GetDetailCell.className)
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 50
        }
    }
    
    @IBOutlet weak var checkVersionOutlet: UIButton!
    @IBOutlet weak var updateVersionOutlet: UIButton!
    
    //MARK:- ViewModel Object
    var DataArrayViewModel = [DetailViewModel]()
    
    //MARK:- Propeties
    var name = ""
    var appId = ""
    var environment = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        setButtonStyle()
        
        let params : [String : AnyObject] = ["app_id": appId as AnyObject]
        let data : [String : AnyObject] = ["data": params as AnyObject]
        getDetail(param: data)
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
   
    
    //MARK:- Response setup with ViewModel
    func getDetail(param: [String:Any]) {
        UserHandler.getDetail(param: param) { [self] (successResponse) in
            if successResponse.message == "Success"{
                
               var data = successResponse.data
                
                
                self.DataArrayViewModel = data?.map({ return DetailViewModel(detail: $0)}) ?? []
                
                self.tableView.reloadData()
            } else {
                let alert = Constants.showAlert(message: successResponse.message)
                self.present(alert, animated: true, completion: nil)
            }
        } failure: { (error) in
            let alert = Constants.showAlert(message: error.message)
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK:- IBActions
    @IBAction func checkVersionBtn(_ sender: Any) {
        print("check version")
        let vcc = storyboard?.instantiateViewController(identifier: "VersionCheckVC")as! VersionCheckVC
        navigationController?.pushViewController(vcc, animated: true)
    }
    @IBAction func updateVersionBtn(_ sender: Any) {
        print("update version")
        let vcv = storyboard?.instantiateViewController(identifier: "UpdateVersionViewController")as!  UpdateVersionViewController // swiftlint:disable:this force_cast
        vcv.appId = appId
        navigationController?.pushViewController(vcv, animated: true)
    }
    
}


//MARK:- Extensions
extension getDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return DataArrayViewModel.count
}
    
    
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: GetDetailCell.className, for: indexPath) as! GetDetailCell // swiftlint:disable:this force_cast
    
    let dataViewModel = DataArrayViewModel[indexPath.row]
    cell.detailViewModel = dataViewModel
    return cell
}

    
    
    
    
}


extension getDetailViewController{
    func setButtonStyle(){
        checkVersionOutlet.layer.borderColor = UIColor(red: 198/255, green: 50/255, blue: 57/225, alpha: 1).cgColor
        checkVersionOutlet.layer.cornerRadius = checkVersionOutlet.frame.size.height/2
        checkVersionOutlet.layer.borderWidth = 1
        checkVersionOutlet.clipsToBounds = true
        updateVersionOutlet.layer.borderColor = UIColor(red: 198/255, green: 50/255, blue: 57/225, alpha: 1).cgColor
        updateVersionOutlet.layer.cornerRadius = updateVersionOutlet.frame.size.height/2
        updateVersionOutlet.layer.borderWidth = 1
        updateVersionOutlet.clipsToBounds = true
    }
    @objc func pullToRefresh() {
    
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            let params: [String : AnyObject] = ["app_id": self.appId as AnyObject]
            let data: [String : AnyObject] = ["data": params as AnyObject]
            self.getDetail(param: data)
            self.tableView.refreshControl?.endRefreshing()
        }
    }
}

//swiftlint: enable all
